return {
    -- Telescope
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = {
        'nvim-lua/plenary.nvim',
        'BurntSushi/ripgrep',
        'sharkdp/fd',
        'nvim-tree/nvim-web-devicons',
        'nvim-telescope/telescope-ui-select.nvim',
        'nvim-telescope/telescope-live-grep-args.nvim'
    },

    config = function()
        require('telescope').setup()
        require('telescope').load_extension('ui-select')
        require('telescope').load_extension("live_grep_args")
    end
}
