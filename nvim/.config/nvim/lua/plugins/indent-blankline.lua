return {
    -- indentation guides
    "lukas-reineke/indent-blankline.nvim",

    config = function()
        require("ibl").setup({
            scope = {
                enabled = true,
            },
            whitespace = {
                remove_blankline_trail = false,
            }
        })
    end
}
