return {
    -- statusline
    'nvim-lualine/lualine.nvim',

    dependencies = {
        "SmiteshP/nvim-navic",
    },

    config = function()
        local navic = require('nvim-navic')
        local base_winbar = function()
            if navic.is_available() then
                local s = navic.get_location()
                if s == nil or s == '' then
                    return " "
                end
                return s
            else
                return " "
            end
        end

        require('lualine').setup({
            options = {
                disabled_filetypes = {
                    statusline = {},
                    winbar = {
                        "help",
                        "startify",
                        "dashboard",
                        "packer",
                        "neogitstatus",
                        "NvimTree",
                        "Trouble",
                        "alpha",
                        "lir",
                        "Outline",
                        "spectre_panel",
                        "toggleterm",
                    }
                },
                refresh = {
                    winbar = 100
                }
            },
            sections = {
                lualine_c = {
                    {
                        'filename',
                        file_status = true,
                        path = 1,
                    }
                },
                lualine_x = {
                    {
                        'encoding'
                    },
                    {
                        'fileformat',
                        symbols = {
                            unix = 'unix',
                            dos = 'win',
                            mac = 'mac'
                        }
                    },
                    {
                        'filetype'
                    }
                }
            },
            inactive_sections = {
                lualine_c = {
                    {
                        'filename',
                        file_status = true,
                        path = 1,
                    }
                }
            },
            winbar = {
                lualine_a = { base_winbar, },
                lualine_b = {},
                lualine_c = {},
                lualine_x = {
                    {
                        require("noice").api.statusline.mode.get,
                        cond = require("noice").api.statusline.mode.has,
                        color = { fg = "#ff9e64" },
                    },
                },
                lualine_y = {},
                lualine_z = {},
            },
            inactive_winbar = {
                lualine_a = { base_winbar, },
                lualine_b = {},
                lualine_c = {},
                lualine_x = {},
                lualine_y = {},
                lualine_z = {},
            },
        })
    end
}
