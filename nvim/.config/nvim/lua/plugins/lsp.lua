local config_mason = function()
    require('mason').setup({
        ui = {
            border = 'single',
            icons = {
                server_installed = "✓",
                server_pending = "➜",
                server_uninstalled = "✗"
            }
        }
    })

    require('mason-lspconfig').setup({
        ensure_installed = {
            'clangd',
            'dockerls',
            'rust_analyzer',
            'lua_ls',
            'eslint@4.7.1'
        }
    })
end

return {
    -- Native LSP for neovim
    'neovim/nvim-lspconfig',
    dependencies = {
        -- LSP/DAP manager
        'williamboman/mason.nvim',
        'williamboman/mason-lspconfig.nvim',

        -- Enable rust-analyzer additional features
        'mrcjkb/rustaceanvim',
    },

    config = function()
        config_mason()

        require('lsp/clangd')
        require('lsp/docker')
        require('lsp/go')
        require('lsp/lua')
        require('lsp/python')
        require('lsp/rust')
        require('lsp/tex')
        require('lsp/typescript')
        require('lsp/zig')

        vim.api.nvim_create_autocmd({ "BufWritePre" },
            {
                callback = function()
                    local ft = vim.bo.filetype
                    if ft == "python" then
                        return
                    end

                    vim.lsp.buf.format()
                end
            }
        )

        require('lspconfig.ui.windows').default_options.border = 'single'
        vim.diagnostic.config({ float = { border = 'single' } })
        vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(
            vim.lsp.handlers.hover, {
                border = "single",
            }
        )
        vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(
            vim.lsp.handlers.signature_help, {
                border = 'single'
            }
        )
    end
}
