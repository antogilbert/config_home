return {
    'Shatur/neovim-tasks',

    -- quickfix
    'romainl/vim-qf',

    -- tabpage interface to cycle through diffs
    {
        'sindrets/diffview.nvim',

        dependencies = {
            'nvim-lua/plenary.nvim'
        },
    },

    -- Highlight same words under cursor.
    'RRethy/vim-illuminate',

    -- VIM SEARCH PULSE - blink when searching words
    'inside/vim-search-pulse',

    -- VIM MARKDOWN
    'gabrielelana/vim-markdown',

    -- VIM-SURROUND, change surrounding characters
    'tpope/vim-surround',
}
