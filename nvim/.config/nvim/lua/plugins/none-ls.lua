return {
    "nvimtools/none-ls.nvim",

    config = function()
        local null_ls = require('null-ls')
        null_ls.setup({
            default_timeout = 10000,
            sources = {
                null_ls.builtins.formatting.prettier,
                -- null_ls.builtins.formatting.prettier_eslint,
                -- null_ls.builtins.formatting.prettierd
            }
        })
    end
}
