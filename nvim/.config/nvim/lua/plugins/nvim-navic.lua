return {
    -- breadcrumbs with function/variable information on status line above
    "SmiteshP/nvim-navic",

    config = function()
        require('nvim-navic').setup()
    end
}
