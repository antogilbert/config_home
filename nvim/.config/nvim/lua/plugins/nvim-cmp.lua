return {
    'hrsh7th/nvim-cmp',
    dependencies = {
        -- display function signatures with current param emphasised
        'hrsh7th/cmp-nvim-lsp-signature-help',

        -- VSCode like pictograms
        'onsails/lspkind.nvim',
        --
        -- LSP completion source for nvim-cmp
        'hrsh7th/cmp-nvim-lsp',

        -- Lua completion source for nvim-cmp
        'hrsh7th/cmp-nvim-lua',

        -- Other usefull completion sources
        'hrsh7th/cmp-path',
        'hrsh7th/cmp-buffer',

        -- Snippet engine
        'L3MON4D3/LuaSnip',
        'saadparwaiz1/cmp_luasnip',
        -- Snippet completion source for nvim-cmd
        -- 'hrsh7th/cmp-vsnip',
        -- 'hrsh7th/vim-vsnip',
        -- 'hrsh7th/vim-vsnip-integ',
    },

    config = function()
        local set = vim.opt

        -- Set completeopt to have a better completion experience
        -- :help completeopt
        -- menuone: popup even when there's only one match
        -- noinsert: Do not insert text until a selection is made
        -- noselect: Do not select, force user to select one from the menu
        set.completeopt = 'menuone,noinsert,noselect'

        -- Avoid showing extra messages when using completion
        set.shortmess:append('c')

        local cmp = require('cmp')

        cmp.setup({
            -- Enable LSP snippets
            snippet = {
                expand = function(args)
                    require('luasnip').lsp_expand(args.body)
                end,
            },
            mapping = {
                ['<Up>'] = cmp.mapping.select_prev_item(),
                ['<Down>'] = cmp.mapping.select_next_item(),
                ['<C-n>'] = cmp.mapping.select_next_item(),
                ['<C-p>'] = cmp.mapping.select_prev_item(),
                -- Add tab support
                -- ['<Tab>'] = cmp.mapping.select_next_item(),
                ['<C-u>'] = cmp.mapping.scroll_docs(-4),
                ['<C-d>'] = cmp.mapping.scroll_docs(4),
                ['<C-Space>'] = cmp.mapping.complete(),
                ['<C-e>'] = cmp.mapping.close(),
                ['<CR>'] = cmp.mapping.confirm({
                    behavior = cmp.ConfirmBehavior.Insert,
                    select = true,
                }),
            },

            -- Installed sources
            sources = {
                { name = 'nvim_lsp' },
                { name = 'nvim_lsp_signature_help' },
                { name = 'luasnip' },
                { name = 'buffer' },
                { name = 'path' },
                { name = 'nvim_lua' },
            },
            window = {
                completion = cmp.config.window.bordered({ border = 'single' }),
                documentation = cmp.config.window.bordered({ border = 'single' }),
            },
            formatting = {
                format = require('lspkind').cmp_format({
                    mode = "symbol_text",
                    menu = ({
                        buffer = "[Buffer]",
                        nvim_lsp = "[LSP]",
                        luasnip = "[LuaSnip]",
                        nvim_lua = "[Lua]",
                    })
                }),
            },
        })

        local ls = require('luasnip')


        local expand_or_jump = function()
            if ls.expand_or_jumpable() then
                ls.expand_or_jump()
            end
        end

        local jump_next = function()
            if ls.jumpable(1) then
                ls.jump(1)
            end
        end

        local jump_prev = function()
            if ls.jumpable(-1) then
                ls.jump(-1)
            end
        end

        local change_choice = function()
            if ls.choice_active() then
                ls.change_choice(1)
            end
        end

        local reload_package = function(package_name)
            for module_name, _ in pairs(package.loaded) do
                if string.find(module_name, '^' .. package_name) then
                    package.loaded[module_name] = nil
                    require(module_name)
                end
            end
        end

        local mode = { 'i', 's' }

        vim.keymap.set(mode, '<C-n>', expand_or_jump)
        vim.keymap.set(mode, '<C-p>', jump_prev)
        vim.keymap.set(mode, '<C-l>', change_choice)
    end
}
