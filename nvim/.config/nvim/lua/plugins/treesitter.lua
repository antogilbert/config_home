return {
    -- Treesitter
    'nvim-treesitter/nvim-treesitter',

    build = ':TSUpdate',

    dependencies = {
        'nvim-treesitter/playground',
        'nvim-treesitter/nvim-treesitter-context',
    },

    config = function()
        require('nvim-treesitter.install').update()
        require('nvim-treesitter.configs').setup({
            ensure_installed = { 'lua', 'rust', 'vimdoc', 'typescript' },
            auto_install = true,
            with_sync = true,
            highlight = {
                enable = true,
                additional_vim_regex_highlighting = false,
            },
            indent = { enable = true },
        })
    end
}
