-- Quickfix magic for errors
return {
    'folke/trouble.nvim',
    config = function()
        local t = require("trouble")
        t.setup()
        vim.keymap.set("n", "<C-q>", ":Trouble diagnostics toggle<CR>", { desc = "Toggle trouble window" })
        vim.keymap.set("n", "<leader>tn", function() t.next({ skip_groups = true, jump = true }) end,
            { desc = "Next entry in trouble quickfix list" })
        vim.keymap.set("n", "<leader>tp", function() t.previous({ skip_groups = true, jump = true }) end,
            { desc = "Prev entry in trouble quickfix list" })
    end
}
