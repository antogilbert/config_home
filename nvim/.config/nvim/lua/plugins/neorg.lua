return {
    'nvim-neorg/neorg',

    version = '*',
    lazy = false,
    config = function()
        require('neorg').setup({
            load = {
                ["core.defaults"] = {},
                ["core.concealer"] = {},
            }
        }
        )
    end
}
