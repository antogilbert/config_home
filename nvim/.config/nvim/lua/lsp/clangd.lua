local navic = require('nvim-navic')

local capabilities = vim.lsp.protocol.make_client_capabilities();
capabilities.textDocument.completion.completionItem.snippetSupport = true

require('lspconfig').clangd.setup({
    on_attach = function(client, bufnr)
        navic.attach(client, bufnr)
        vim.keymap.set('n', '<leader>hs', '<cmd>ClangdSwitchSourceHeader<cr>',
            { noremap = true, desc = "Switch between [h]eader and impl in C++ [s]ource files" })
        client.server_capabilities.semanticTokensProvider = nil
    end,
    capabilities = capabilities,
    cmd = {
        "clangd",
        "--clang-tidy",
        "--clang-tidy-checks=cert-dcl59-cpp",
        "--background-index",
    },
})
