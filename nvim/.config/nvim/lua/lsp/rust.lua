local navic = require('nvim-navic')

vim.g.rustaceanvim = {
    hover_actions = {
        auto_focus = true
    },
    server = {
        on_attach = function(client, bufnr)
            client.server_capabilities.semanticTokensProvider = nil
            navic.attach(client, bufnr)
            -- Code action groups
            -- vim.keymap.set("n", "<Leader>ca", rt.code_action_group.code_action_group, { buffer = bufnr })
            vim.keymap.set("n", "<Leader>oc", vim.cmd.RustOpenCargo, { buffer = bufnr })
            vim.keymap.set("n", "<Leader>rd", vim.cmd.RustDebuggables, { buffer = bufnr })
            vim.keymap.set("n", "<Leader>rr", vim.cmd.RustRunnables, { buffer = bufnr })
            vim.keymap.set("n", "<Leader>em", vim.cmd.RustExpandMacro, { buffer = bufnr })
            vim.keymap.set("n", "<Leader>mu", vim.cmd.RustMoveItemUp, { buffer = bufnr })
            vim.keymap.set("n", "<Leader>md", vim.cmd.RustMoveItemDown, { buffer = bufnr })
        end,
        settings = {
            ["rust-analyzer"] = {
                assist = {
                    importGranularity = "module",
                    importPrefix = "self",
                },
                cargo = {
                    loadOutDirsFromCheck = true,
                    features = "all",
                },
                check = {
                    features = "all",
                    command = "clippy"
                },
                procMacro = {
                    enable = true
                },
            }
        }
        --        cmd = { "/Users/antonello/rust/rust-analyzer/target/release/rust-analyzer" },
    },
    dap = {
        autoload_configurations = true
    }
}
