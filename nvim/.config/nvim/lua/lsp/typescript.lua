local navic = require('nvim-navic')

-- require('lspconfig').denols.setup({
require('lspconfig').ts_ls.setup({
    on_attach = function(client, bufnr)
        navic.attach(client, bufnr)
        client.server_capabilities.semanticTokensProvider = nil
    end,
    flags = { allow_incremental_sync = true, debounce_text_changes = 500 },
    settings = {
        javascript = {
            inlayHints = {
                includeInlayEnumMemberValueHints = true,
                includeInlayFunctionLikeReturnTypeHints = true,
                includeInlayFunctionParameterTypeHints = true,
                includeInlayParameterNameHints = "all", -- 'none' | 'literals' | 'all';
                includeInlayParameterNameHintsWhenArgumentMatchesName = true,
                includeInlayPropertyDeclarationTypeHints = true,
                includeInlayVariableTypeHints = true,
            },
        },
        typescript = {
            inlayHints = {
                includeInlayEnumMemberValueHints = true,
                includeInlayFunctionLikeReturnTypeHints = true,
                includeInlayFunctionParameterTypeHints = true,
                includeInlayParameterNameHints = "all", -- 'none' | 'literals' | 'all';
                includeInlayParameterNameHintsWhenArgumentMatchesName = true,
                includeInlayPropertyDeclarationTypeHints = true,
                includeInlayVariableTypeHints = true,
            },
        },
    }
})

require('lspconfig').eslint.setup({
    on_attach = function(client)
        client.server_capabilities.documentFormattingProvider = true
    end,
})
