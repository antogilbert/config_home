local navic = require('nvim-navic')

require('lspconfig').jedi_language_server.setup({
    on_attach = function(client, bufnr)
        navic.attach(client, bufnr)
    end,
})

require('lspconfig').ruff.setup({})
