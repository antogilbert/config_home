local nvim_lsp = require('lspconfig')
local navic = require('nvim-navic')

nvim_lsp.buck2.setup({
    cmd = { "/home/antonello/software/buck2/buck2", "lsp" },
    on_attach = function(client, bufnr)
        require 'completion'.on_attach(client)
        navic.attach(client, bufnr)
    end,
})

vim.cmd [[ autocmd BufRead,BufNewFile *.bxl,BUCK,TARGETS set filetype=bzl ]]
