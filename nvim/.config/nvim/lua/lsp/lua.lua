local navic = require('nvim-navic')

require('lspconfig').lua_ls.setup({
    on_attach = function(client, bufnr)
        navic.attach(client, bufnr)
        client.server_capabilities.semanticTokensProvider = nil
    end,
    settings = {
        Lua = {
            diagnostics = {
                globals = { 'vim' }
            }
        }
    }
})
