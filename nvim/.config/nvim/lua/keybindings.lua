-- leader is mapped in lazy_init because it is required by lazy
-- vim.g.mapleader = " "

local nmap = function(keys, func, desc)
    if desc then
        desc = "LSP: " .. desc
    end

    vim.keymap.set("n", keys, func, { desc = desc })
end

local builtin = require("telescope.builtin")

--  LSP
nmap("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")
nmap("<leader>a", vim.lsp.buf.code_action, "Code [A]ction")
nmap("<leader>fm", vim.lsp.buf.format, "[F]or[m]at buffer")
nmap("<leader>gd", vim.lsp.buf.definition, "[G]oto [D]efinition")
nmap("<leader>gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
nmap("<leader>gi", vim.lsp.buf.implementation, "[G]oto [I]mplementation")
nmap("<leader>gr", builtin.lsp_references, "[G]oto [R]eferences")
nmap("<leader>ds", builtin.lsp_document_symbols, "[D]ocument [S]ymbols")
nmap("<leader>ws", builtin.lsp_dynamic_workspace_symbols, "[W]orkspace [S]ymbols")
-- See `:help K` for why this keymap

nmap("K", vim.lsp.buf.hover, "Hover Documentation")
nmap("<leader>ss", vim.lsp.buf.signature_help, "[S]how [S]ignature Documentation")
nmap("<leader>k", vim.diagnostic.open_float, "Open diagnostics floating panel")
nmap("<leader>wa", vim.lsp.buf.add_workspace_folder, "[W]orkspace [A]dd Folder")
nmap("<leader>wr", vim.lsp.buf.remove_workspace_folder, "[W]orkspace [R]emove Folder")
nmap("<leader>wl", function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
end, "[W]orkspace [L]ist Folders")
nmap("<leader>ch", vim.lsp.buf.incoming_calls, "[C]all [H]ierarchy")
nmap("<Leader>ih", function() vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled()) end,
    "Toggle [I]nlay [H]ints")


vim.keymap.set("i", "<C-h>", vim.lsp.buf.signature_help, { desc = "[S]how [S]ignature Documentation" })
-- NEOVIM remaps
vim.keymap.set("n", "<F8>", ":vsplit<CR>:term<CR>", { noremap = true, desc = "Open terminal in vsplit" })
vim.keymap.set("t", "<C-\\>", "<C-\\><C-n>", { noremap = true, desc = "Exit from insert mode inside terminal" })

--  General
vim.keymap.set("n", "<F6>", vim.cmd.tabe, { noremap = true, desc = "Open new tab" })
vim.keymap.set("n", "<F7>", vim.cmd.tabc, { noremap = true, desc = "Close current tab" })

-- Remaps for  C-d/C-u
vim.keymap.set("n", "<C-d", "<C-d>zz", { noremap = true, desc = "Remap to have cursor at centre of page" })
vim.keymap.set("n", "<C-u>", "<C-u>zz", { noremap = true, desc = "Remap to have cursor at centre of page" })

--  VIM-QF - Improved quickfix list
vim.keymap.set("n", "cn", "<Plug>(qf_qf_next)", { desc = "Go to next entry in quickfix list" })
vim.keymap.set("n", "cp", "<Plug>(qf_qf_previous)", { desc = "Go to prev entry in quickfix list" })
vim.keymap.set("n", "co", "<Plug>(qf_qf_toggle)", { desc = "Toggle quickfix list" })
vim.keymap.set("n", "ck", ":Keep ", { desc = "Keep in quickfix list" })
vim.keymap.set("n", "cr", ":Reject ", { desc = "Remove from quickfix list" })
vim.keymap.set("n", "cu", vim.cmd.Restore, { desc = "Restore original content of quickfix list" })

-- visual to visual block mode
vim.keymap.set("n", "<leader>v", "<C-v>", { noremap = true, desc = "Remap for visual block mode" })

vim.keymap.set("n", "<leader>m", vim.cmd.Mason, { desc = "Open Mason" })

--  Telescope - FUZZY FINDER
vim.keymap.set("n", "<leader>fw", builtin.grep_string, { desc = "[F]ind [W]ord under cursor" })
vim.keymap.set("n", "<leader>sd", builtin.diagnostics, { desc = "[S]earch [D]iagnostics" })
vim.keymap.set("n", "<leader>fp", builtin.git_files, { desc = "Ctrl-P like VSCode (Git Files)" })
vim.keymap.set("n", "<leader>ff", builtin.find_files, { desc = "[F]ind [F]iles" })
vim.keymap.set("n", "<leader>fg", builtin.live_grep, { desc = "[F]ile search by [G]rep" })
vim.keymap.set("n", "<leader>fb", builtin.buffers, { desc = "[F]ind [B]uffers" })
vim.keymap.set("n", "<leader>fh", builtin.help_tags, { desc = "[F]ind [H]elp" })
vim.keymap.set("n", "<leader>km", builtin.keymaps, { desc = "[K]ey [M]appings" })

-- Diffview open diff window
vim.keymap.set("n", "<leader>od", vim.cmd.DiffviewOpen, { noremap = true, desc = "Open git diff view for current file" })
vim.keymap.set("n", "<leader>cd", vim.cmd.DiffviewClose,
    { noremap = true, desc = "Close git diff view for current file" })


-- Paste over selection and preserve original paste
vim.keymap.set("x", "<leader>p", "\"_dP", { desc = "Paste over selection and preserve original paste" })

-- Delete and preserve original buffer content
vim.keymap.set("n", "<leader>d", "\"_d", { desc = "Delete and preserve original buffer content" })
vim.keymap.set("v", "<leader>d", "\"_d", { desc = "Delete and preserve original buffer content" })

-- Copy to system clipboard
vim.keymap.set("n", "<leader>y", "\"+y", { desc = "Copy to system clipboard" })
vim.keymap.set("v", "<leader>y", "\"+y", { desc = "Copy to system clipboard" })
vim.keymap.set("n", "<leader>Y", "\"+Y", { desc = "Copy full line to system clipboard" })

-- Move selected block up and down
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv", { desc = "Move selected block down" })
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv", { desc = "Move selected block up" })

vim.keymap.set("n", "<leader>td", vim.cmd.TodoTelescope, { desc = "Open all TODOs in telescope" })

vim.keymap.set("n", "<leader>lr", vim.cmd.LspRestart, { desc = "Restart LSP" })

-- dap
vim.keymap.set("n", "<leader>dc", ":DapContinue<CR>", { desc = "Start/continue debugger" })
vim.keymap.set("n", "<leader>db", ":DapToggleBreakpoint<CR>", { desc = "Toggle breakpoint on current line" })
vim.keymap.set("n", "<leader>dx", ":DapTerminate<CR>", { desc = "Terminate debugger" })
vim.keymap.set("n", "<leader>dn", ":DapStepOver<CR>", { desc = "Next step on debugger" })

vim.keymap.set("n", "<leader>tt", "<CMD>AerialToggle!<CR>", {})
vim.keymap.set("n", "<leader>tc", "<CMD>AerialCloseAll<CR>", {})
