local set          = vim.opt

--GENERAL OPTIONS
set.mouse          =
'a'                                                                -- Allows you to click around the text editor with your mouse to move the cursor

set.guicursor      = 'a:block-blinkwait300-blinkon200-blinkoff150' -- disable different cursors for different modes
set.cursorline     = true

set.showcmd        = true                                    -- Show partial commands in last line of screen
set.showmatch      = true                                    -- Highlights matching brackets in programming languages
set.number         = true                                    -- Enables line numbering
set.relativenumber = true                                    -- Relative numbering of lines in normal mode
set.backspace      = { 'indent', 'eol', 'start' }            -- This makes the backspace key function like it does in other programs
set.wrap           = false                                   -- Display long lines as just one line
set.list           = true
set.listchars      = { tab = '>·', trail = '·', nbsp = '·' } -- Prints tabs and trailing spaces
set.encoding       = 'utf8'                                  -- Character encoding
set.cc             = '101'                                   -- Show 100 characters limit
set.wildmenu       = true                                    -- Shows list of completions of commands
set.updatetime     = 1000                                    -- updates every second instead of every 4 (default)
set.swapfile       = false                                   -- Don't use swap files

--SEARCH OPTIONS
set.hlsearch       = true -- Highlights searches
set.ignorecase     = true -- Use case insensitive search...
set.smartcase      = true -- ...except when using capital letters
set.incsearch      = true -- Show search matches as you type

vim.api.nvim_create_autocmd({ "FileType" }, {
    pattern = { "*.c", "*.cpp", "*.tex", "*.bib", "*.sh" },
    command = "vim.opt.noignorecase = true"
}) --For those files deactivate ignorecase

--INDENTATION OPTIONS
set.autoindent = true
set.shiftround = true  -- Uses number in shiftwidth to indent with > or <
set.copyindent = true  -- Copy the previous indentation
set.smartindent = true -- Automatically indents lines after opening a bracket in programming languages

--TAB OPTIONS
set.shiftwidth = 4   -- How much space Vim gives to autoindent
set.tabstop = 4      -- How much space Vim gives to a tab
set.expandtab = true -- Converts tabs to spaces
set.smarttab = true  -- Insert tabs at start of line according to shiftwidth not tabstop

vim.g.markdown_enable_spell_checking = 0
--SPELL CHECKER
vim.api.nvim_create_autocmd({ "FileType" }, {
    pattern = { "*.tex" },
    command = "vim.opt.spell = true; vim.opt.spelllang = 'en_gb'"
})

set.termguicolors = true

set.signcolumn = 'yes'

vim.api.nvim_set_hl(0, 'Whitespace', { fg = "#616161" })
vim.api.nvim_set_hl(0, 'WinSeparator', { fg = "#616161" })

-- clipboard with win32yank.exe
-- in ~/bin/win32yank.exe
-- https://github.com/equalsraf/win32yank/releases {{{

if vim.fn.has('wsl') == 1 then
    vim.g.clipboard = {
        name = "win32yank-wsl",
        copy = {
            ["+"] = "win32yank -i",
            ["*"] = "win32yank -i"
        },
        paste = {
            ["+"] = "win32yank -o",
            ["*"] = "win32yank -o"
        },
        cache_enable = 0,
    }
end
-- }}}
