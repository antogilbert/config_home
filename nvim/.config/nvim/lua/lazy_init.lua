vim.g.mapleader = " "
require('lazy').setup({
    spec = 'plugins',
    ui = {
        border = 'single'
    }
})
