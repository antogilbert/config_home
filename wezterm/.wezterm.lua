-- Pull in the wezterm API
local wezterm = require 'wezterm'
local act = wezterm.action

-- This will hold the configuration.
local config = wezterm.config_builder()

config.color_scheme = 'Afterglow'
config.font = wezterm.font("CaskaydiaCove Nerd Font Mono")
config.font_size = 13.5

config.keys = {
    -- Rebind CTRL-Left, CTRL-Right as ALT-b, ALT-f respectively to match Terminal.app behavior
    {
        key = 'LeftArrow',
        mods = 'CTRL',
        action = act.SendKey {
            key = 'b',
            mods = 'ALT',
        },
    },
    {
        key = 'RightArrow',
        mods = 'CTRL',
        action = act.SendKey {
            key = 'f',
            mods = 'ALT'
        },
    },
}

config.freetype_load_target = "Light"
config.window_close_confirmation = 'NeverPrompt'

config.default_cursor_style = "BlinkingBlock"
config.cursor_blink_rate = 500
config.cursor_blink_ease_in = "Constant"
config.cursor_blink_ease_out = "Constant"

-- and finally, return the configuration to wezterm
return config
