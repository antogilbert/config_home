#==============================================================================
# MISC
#==============================================================================

#directories in bold

if [ "$(uname 2> /dev/null)" != "Linux" ]; then
eval $(gdircolors -b)
export CLICOLOR=1
else
eval $(dircolors -b)
alias ls="ls --color=auto"
fi

export TERM='xterm-256color'

# quiet
setopt no_beep

# extended globbing
setopt extendedglob

# disable mail checking in terminal
unset MAILCHECK

# background processes aren't killed on exit of shell
setopt autocontinue

#==============================================================================
# STANDARD KEYS BEHAVIOUR
#==============================================================================
bindkey "\e[H" beginning-of-line
bindkey "\e[F" end-of-line
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[2~" quoted-insert
bindkey "\e[3~" delete-char
