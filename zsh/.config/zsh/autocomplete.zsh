#==============================================================================
# AUTOCOMPLETION
#==============================================================================
autoload -U compinit ; compinit
setopt complete_in_word
if [ "$(uname 2> /dev/null)" != "Linux" ]; then
zstyle ':completion:*' menu select=2 eval "$(gdircolors -b)"
else
zstyle ':completion:*' menu select=2 eval "$(dircolors -b)"
fi
zstyle ':completion:*:cd:*' ignore-parents parent pwd
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' format  "%F{green}Completing ---> %B%d%b%f"
zstyle ':completion:*' group-name ''

zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

