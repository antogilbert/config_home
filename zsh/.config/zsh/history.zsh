#==============================================================================
# HISTORY OPTIONS
#==============================================================================
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=$HOME/.zsh_history
setopt hist_ignore_all_dups
setopt hist_reduce_blanks
setopt appendhistory
setopt share_history
bindkey '^[OA' history-beginning-search-backward
bindkey '^[OB' history-beginning-search-forward
bindkey '^[[A' history-beginning-search-backward
bindkey '^[[B' history-beginning-search-forward
