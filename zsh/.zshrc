#! /bin/zsh

#==============================================================================
# PROMPT/TITLEBAR/COLORS/TERMINAL BEHAVIOUR
#==============================================================================
source $HOME/.config/zsh/terminal.zsh

#==============================================================================
# AUTOCOMPLETION
#==============================================================================
source $HOME/.config/zsh/autocomplete.zsh

#==============================================================================
# HISTORY OPTIONS
#==============================================================================
source $HOME/.config/zsh/history.zsh

#==============================================================================
# Git prompts - taken from BUREAU theme of OH-MY-ZSH
#==============================================================================
# source $HOME/config_home/zsh/git_prompts.zsh

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

PATH="$HOME/dev/pmu-tools/:$PATH"
PATH="$HOME/dev/runperf/:$PATH"
PATH="$HOME/software/buck2/:$PATH"
PATH="$HOME/software/balena-etcher/:$PATH"
PATH="$HOME/software/vial/:$PATH"

. "$HOME/.cargo/env"

export EDITOR="/usr/bin/vim"
#export DOCKER_HOST=tcp://0.0.0.0:2375
#

eval "$(starship init zsh)"
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
